import {LanguageModel} from '../language-model';

export const langNo: LanguageModel = {
  product: 'Tailwind', // safe
  company: 'Carl Fearby', // safe
  desktop: {
    ribbon: {
      contacts: '',
      settings: '',
      someLongerText: '',
      catalogues: '',
      multiSelect: '',
      ghost: '',
      planMode1: '',
      planMode2: '',
      planMode3: '',
      planMode4: '',
      deleteRecord: '',
      saveRecord: '',
      addRecord: '',
      sendEmail: '',
      editSettings: '',
      cancelAddingRecord: '',
      discardChanges: '',
      notesAndHistory: '',
      supportTickets: '',
      accounts: '',
      contactProfile: '',
      documentsAndFiles: '',
    },
    menu: {
      menu1: '',
      menu2: '',
      menu3: '',
      menu4: '',
    },
    window: {
      close: '',
      minimise: '',
      maximise: '',
      centre: '',
      restore: '',
      loading: '',
      dockTop: '',
      dockRight: '',
      dockBottom: '',
      dockLeft: '',
      dockTopLeft: '',
      dockTopRight: '',
      dockBottomLeft: '',
      dockBottomRight: '',
      unDockWindow: '',
      moveWindow: '',
      top: '',
      width: '',
      left: '',
      height: '',
      resetWindowPosition: '',
      login: '',
      newWindow: '',
      profile: '',
      setDockPosition: '',
      unsaved: '',
      placeInPanel: '',
      removeFromPanel: '',
      about: '',
      demo: '',
      demo1: '',
      demo2: '',
      demo3: '',
      demo4: '',
      contactManager: '',
      addHistory: '',
      addSupportTicket: '',
      editSettings: '',
      search: '',
      editCategoryItem: '',
      editGroupItem: '',
      editStatusItem: '',
      editTypeItem: '',
      addCategoryItem: '',
      addGroupItem: '',
      addStatusItem: '',
      addTypeItem: '',
      notesHistory: '',
      supportTickets: '',
      planner: '',
    },
    panel: {
      close: '',
      dockTop: '',
      dockRight: '',
      dockBottom: '',
      dockLeft: '',
      dockTopLeft: '',
      dockTopRight: '',
      dockBottomLeft: '',
      dockBottomRight: '',
      unDockPanel: '',
      moveWindow: '',
      top: '',
      width: '',
      left: '',
      height: '',
      setDockPosition: '',
      system: '',
    },
    toast: {
      close: '',
      dismiss: '',
      success: '',
      warning: '',
      information: '',
      error: '',
      loginSuccessful: '',
      logoutSuccessful: '',
      loginFailed: '',
    },
    dialog: {
      ok: '',
      cancel: '',
      close: '',
      yes: '',
      no: '',
      agree: '',
      disagree: '',
      decline: '',
      closeWindow: '',
      youHaveUnsavedContentAreYouSureYouWantToCloseThisWindow: '',
      newPanel: '',
      pleaseSpecifyYourPanelName: '',
      panelName: '',
      left: '',
      width: '',
      createPanel: '',
      closePanel: '',
      doYouWantToCloseTheWindows: '',
      keepWindows: '',
      closeWindows: '',
      discardChanges: '',
      areYouSureYouWishToDiscardYourChanges: '',
      discard: '',
      deleteRecord: '',
      areYouSureYouWishToDeleteThisRecord: '',
      delete: '',
    },
    tabBar: {
      autoHide: '',
    },
    newWindow: '',
    loading: '',
  },
  components: {
    languageSelector: {
      language: 'langNo',
      languages: {
        langEn: 'English', // safe
        langFr: 'Français', // safe
        langDe: 'Deutsch', // safe
        langNo: 'Norsk', // safe
        langRu: 'Russkiy', // safe
        langNl: 'Nederlands', // safe
        langEs: 'Español', // safe
        langIt: 'Italiano', // safe
        langDa: 'Dansk', // safe
        langSv: 'Svenska', // safe
        langPt: 'Português', // safe
        langFi: 'Suomi', // safe
        langZh: '中文', // safe
        langPl: 'Polskie' // safe
      },
      flags: {
        langEn: 'united-kingdom.png', // safe
        langFr: 'france.png', // safe
        langDe: 'germany.png', // safe
        langNo: 'norway.png', // safe
        langRu: 'russia.png', // safe
        langNl: 'netherlands.png', // safe
        langEs: 'spain.png', // safe
        langIt: 'italy.png', // safe
        langDa: 'denmark.png', // safe
        langSv: 'sweden.png', // safe
        langPt: 'portugal.png', // safe
        langFi: 'finland.png', // safe
        langZh: 'china.png', // safe
        langPl: 'republic-of-poland.png' // safe
      }
    },
    settings: {
      settings: '',
      ribbonSize: '',
      tabBarAutoHide: '',
      small: '',
      large: '',
    },
    videoPlayer: {
      play: '',
      pause: '',
      volume: '',
      fullScreen: '',
      exitFullScreen: '',
      skipBack: '',
      skipForward: '',
      mute: '',
      unMute: '',
      pictureInPicture: '',
      errorLoadingVideo: '',
      restart: '',
      settings: '',
    }
  },
  system: {
    example: {
      test: '',
    },
    engine: {},
    userLogin: {
      username: '',
      password: '',
      login: '',
      pleaseWaitLoggingIn: '',
    },
    userMenu: {
      profile: '',
      logout: '',
    },
    splash: {
      close: '',
      autoCloseIn: '',
    },
    about: {
      close: '',
    }
  },
  contactManager: {
    noRecordsFound: '',
    recordChanged: '',
    addingNewRecord: '',
    ofCount: '',
    editSettings: '',
    pleaseSelect: '',
    advancedSearch: '',
    switchView: '',
    name: '',
    forename: '',
    surname: '',
    address: '',
    town: '',
    county: '',
    postcode: '',
    lookup: '',
    country: '',
    jobTitle: '',
    department: '',
    work: '',
    fax: '',
    mobile: '',
    email: '',
    group: '',
    category: '',
    updateRecord: '',
    newRecord: '',
    deleteRecord: '',
    created: '',
    updated: '',
    lastUpdatedBy: '',
    lastLogin: '',
    start: '',
    previous50: '',
    previous: '',
    next: '',
    next50: '',
    end: '',
    categories: '',
    accountNumber: '',
    company: '',
    type: '',
    status: '',
    website: '',
    contactAssociations: '',
    general: '',
    authentication: '',
    otherInfo: '',
    companySettings: '',
    access: '',
    username: '',
    password: '',
    allowLogin: '',
    notesAndHistory: '',
    supportTickets: '',
    accounts: '',
    contactProfile: '',
    documentsAndFiles: '',
    searching: '',
    selectAddress: '',
    addressNotListed: '',
    pleaseWaitLoading: '',
    readOnly: '',
    public: '',
    private: '',
    addedBy: '',
    updatedBy: '',
    Administrator: '',
    Moderator: '',
    Manager: '',
    Supervisor: '',
    User: '',
    None: '',
    Opportunity: '',
    Prospect: '',
    'Not Contacted': '',
    'Appointment Booked': '',
    '2nd Appointment Booked': '',
    'Awaiting Decision': '',
    Customer: '',
    Staff: '',
    Contacts: '',
    Suppliers: '',
    Live: '',
    Lead: '',
    contactRecords: '',
    companyRecords: '',
    yourRecordHasBeenSaved: '',
    pleaseEnterALongerSurname: '',
    pleaseEnterALongerCompanyName: '',
    pleaseSpecifyUserAuthDetails: '',
    yourRecordHasBeenDeleted: '',
    notesAndHistoryTab: {
      addNotesOrHistory: '',
      addSupportTicket: '',
      type: '',
      date: '',
      time: '',
      contact: '',
      agent: '',
      popOut: '',
      noteComponent: {
        notes: '',
        supportCall: '',
        openNote: '',
        openSupportTicket: '',
      }
    },
    accountsTab: {
      ok: '',
    },
    contactProfileTab: {
      ok: '',
    },
    documentsAndFilesTab: {
      ok: '',
    },
    settings: {
      categories: '',
      types: '',
      groups: '',
      status: '',
      addNewCategory: '',
      addNewContactCategory: '',
      addNewCompanyCategory: '',
      contactCategories: '',
      companyCategories: '',
      addNewStatus: '',
      addNewGroup: '',
      addNewType: '',
      setDefault: '',
      item: {
        cancel: '',
        save: '',
        CategoryName: '',
        GroupName: '',
        StatusName: '',
        TypeName: '',
        backgroundGradientTopColour: '',
        backgroundGradientBottomColour: '',
        textColour: '',
        fieldTooShort: '',
      }
    },
  },
  countries: {
    AF: 'Afghanistan', // safe
    AL: 'Albania', // safe
    DZ: 'Algeria', // safe
    AS: 'American Samoa', // safe
    AD: 'Andorra', // safe
    AO: 'Angola', // safe
    AI: 'Anguilla', // safe
    AQ: 'Antarctica', // safe
    AG: 'Antigua and Barbuda', // safe
    AR: 'Argentina', // safe
    AM: 'Armenia', // safe
    AW: 'Aruba', // safe
    AU: 'Australia', // safe
    AT: 'Austria', // safe
    AZ: 'Azerbaijan', // safe
    BS: 'Bahamas', // safe
    BH: 'Bahrain', // safe
    BD: 'Bangladesh', // safe
    BB: 'Barbados', // safe
    BY: 'Belarus', // safe
    BE: 'Belgium', // safe
    BZ: 'Belize', // safe
    BJ: 'Benin', // safe
    BM: 'Bermuda', // safe
    BT: 'Bhutan', // safe
    BO: 'Bolivia (Plurinational State of)', // safe
    BQ: 'Bonaire, Sint Eustatius and Saba', // safe
    BA: 'Bosnia and Herzegovina', // safe
    BW: 'Botswana', // safe
    BV: 'Bouvet Island', // safe
    BR: 'Brazil', // safe
    IO: 'British Indian Ocean Territory', // safe
    BN: 'Brunei Darussalam', // safe
    BG: 'Bulgaria', // safe
    BF: 'Burkina Faso', // safe
    BI: 'Burundi', // safe
    CV: 'Cabo Verde', // safe
    KH: 'Cambodia', // safe
    CM: 'Cameroon', // safe
    CA: 'Canada', // safe
    KY: 'Cayman Islands', // safe
    CF: 'Central African Republic', // safe
    TD: 'Chad', // safe
    CL: 'Chile', // safe
    CN: 'China', // safe
    CX: 'Christmas Island', // safe
    CC: 'Cocos (Keeling) Islands', // safe
    CO: 'Colombia', // safe
    KM: 'Comoros', // safe
    CD: 'Congo (the Democratic Republic of the)', // safe
    CG: 'Congo', // safe
    CK: 'Cook Islands', // safe
    CR: 'Costa Rica', // safe
    HR: 'Croatia', // safe
    CU: 'Cuba', // safe
    CW: 'Curaçao', // safe
    CY: 'Cyprus', // safe
    CZ: 'Czechia', // safe
    CI: 'Côte d\'Ivoire', // safe
    DK: 'Denmark', // safe
    DJ: 'Djibouti', // safe
    DM: 'Dominica', // safe
    DO: 'Dominican Republic', // safe
    EC: 'Ecuador', // safe
    EG: 'Egypt', // safe
    SV: 'El Salvador', // safe
    GQ: 'Equatorial Guinea', // safe
    ER: 'Eritrea', // safe
    EE: 'Estonia', // safe
    SZ: 'Eswatini', // safe
    ET: 'Ethiopia', // safe
    FK: 'Falkland Islands [Malvinas]', // safe
    FO: 'Faroe Islands', // safe
    FJ: 'Fiji', // safe
    FI: 'Finland', // safe
    FR: 'France', // safe
    GF: 'French Guiana', // safe
    PF: 'French Polynesia', // safe
    TF: 'French Southern Territories', // safe
    GA: 'Gabon', // safe
    GM: 'Gambia', // safe
    GE: 'Georgia', // safe
    DE: 'Germany', // safe
    GH: 'Ghana', // safe
    GI: 'Gibraltar', // safe
    GR: 'Greece', // safe
    GL: 'Greenland', // safe
    GD: 'Grenada', // safe
    GP: 'Guadeloupe', // safe
    GU: 'Guam', // safe
    GT: 'Guatemala', // safe
    GG: 'Guernsey', // safe
    GN: 'Guinea', // safe
    GW: 'Guinea-Bissau', // safe
    GY: 'Guyana', // safe
    HT: 'Haiti', // safe
    HM: 'Heard Island and McDonald Islands', // safe
    VA: 'Holy See', // safe
    HN: 'Honduras', // safe
    HK: 'Hong Kong', // safe
    HU: 'Hungary', // safe
    IS: 'Iceland', // safe
    IN: 'India', // safe
    ID: 'Indonesia', // safe
    IR: 'Iran (Islamic Republic of)', // safe
    IQ: 'Iraq', // safe
    IE: 'Ireland', // safe
    IM: 'Isle of Man', // safe
    IL: 'Israel', // safe
    IT: 'Italy', // safe
    JM: 'Jamaica', // safe
    JP: 'Japan', // safe
    JE: 'Jersey', // safe
    JO: 'Jordan', // safe
    KZ: 'Kazakhstan', // safe
    KE: 'Kenya', // safe
    KI: 'Kiribati', // safe
    KP: 'Korea (the Democratic People\'s Republic of)', // safe
    KR: 'Korea (the Republic of)', // safe
    KW: 'Kuwait', // safe
    KG: 'Kyrgyzstan', // safe
    LA: 'Lao People\'s Democratic Republic', // safe
    LV: 'Latvia', // safe
    LB: 'Lebanon', // safe
    LS: 'Lesotho', // safe
    LR: 'Liberia', // safe
    LY: 'Libya', // safe
    LI: 'Liechtenstein', // safe
    LT: 'Lithuania', // safe
    LU: 'Luxembourg', // safe
    MO: 'Macao', // safe
    MG: 'Madagascar', // safe
    MW: 'Malawi', // safe
    MY: 'Malaysia', // safe
    MV: 'Maldives', // safe
    ML: 'Mali', // safe
    MT: 'Malta', // safe
    MH: 'Marshall Islands', // safe
    MQ: 'Martinique', // safe
    MR: 'Mauritania', // safe
    MU: 'Mauritius', // safe
    YT: 'Mayotte', // safe
    MX: 'Mexico', // safe
    FM: 'Micronesia (Federated States of)', // safe
    MD: 'Moldova (the Republic of)', // safe
    MC: 'Monaco', // safe
    MN: 'Mongolia', // safe
    ME: 'Montenegro', // safe
    MS: 'Montserrat', // safe
    MA: 'Morocco', // safe
    MZ: 'Mozambique', // safe
    MM: 'Myanmar', // safe
    NA: 'Namibia', // safe
    NR: 'Nauru', // safe
    NP: 'Nepal', // safe
    NL: 'Netherlands', // safe
    NC: 'New Caledonia', // safe
    NZ: 'New Zealand', // safe
    NI: 'Nicaragua', // safe
    NE: 'Niger', // safe
    NG: 'Nigeria', // safe
    NU: 'Niue', // safe
    NF: 'Norfolk Island', // safe
    MK: 'North Macedonia', // safe
    MP: 'Northern Mariana Islands', // safe
    NO: 'Norway', // safe
    OM: 'Oman', // safe
    PK: 'Pakistan', // safe
    PW: 'Palau', // safe
    PS: 'Palestine, State of', // safe
    PA: 'Panama', // safe
    PG: 'Papua New Guinea', // safe
    PY: 'Paraguay', // safe
    PE: 'Peru', // safe
    PH: 'Philippines', // safe
    PN: 'Pitcairn', // safe
    PL: 'Poland', // safe
    PT: 'Portugal', // safe
    PR: 'Puerto Rico', // safe
    QA: 'Qatar', // safe
    RO: 'Romania', // safe
    RU: 'Russian Federation', // safe
    RW: 'Rwanda', // safe
    RE: 'Réunion', // safe
    BL: 'Saint Barthélemy', // safe
    SH: 'Saint Helena, Ascension and Tristan da Cunha', // safe
    KN: 'Saint Kitts and Nevis', // safe
    LC: 'Saint Lucia', // safe
    MF: 'Saint Martin (French part)', // safe
    PM: 'Saint Pierre and Miquelon', // safe
    VC: 'Saint Vincent and the Grenadines', // safe
    WS: 'Samoa', // safe
    SM: 'San Marino', // safe
    ST: 'Sao Tome and Principe', // safe
    SA: 'Saudi Arabia', // safe
    SN: 'Senegal', // safe
    RS: 'Serbia', // safe
    SC: 'Seychelles', // safe
    SL: 'Sierra Leone', // safe
    SG: 'Singapore', // safe
    SX: 'Sint Maarten (Dutch part)', // safe
    SK: 'Slovakia', // safe
    SI: 'Slovenia', // safe
    SB: 'Solomon Islands', // safe
    SO: 'Somalia', // safe
    ZA: 'South Africa', // safe
    GS: 'South Georgia and the South Sandwich Islands', // safe
    SS: 'South Sudan', // safe
    ES: 'Spain', // safe
    LK: 'Sri Lanka', // safe
    SD: 'Sudan', // safe
    SR: 'Suriname', // safe
    SJ: 'Svalbard and Jan Mayen', // safe
    SE: 'Sweden', // safe
    CH: 'Switzerland', // safe
    SY: 'Syrian Arab Republic', // safe
    TW: 'Taiwan (Province of China)', // safe
    TJ: 'Tajikistan', // safe
    TZ: 'Tanzania, the United Republic of', // safe
    TH: 'Thailand', // safe
    TL: 'Timor-Leste', // safe
    TG: 'Togo', // safe
    TK: 'Tokelau', // safe
    TO: 'Tonga', // safe
    TT: 'Trinidad and Tobago', // safe
    TN: 'Tunisia', // safe
    TR: 'Turkey', // safe
    TM: 'Turkmenistan', // safe
    TC: 'Turks and Caicos Islands', // safe
    TV: 'Tuvalu', // safe
    UG: 'Uganda', // safe
    UA: 'Ukraine', // safe
    AE: 'United Arab Emirates', // safe
    GB: 'United Kingdom', // safe
    UM: 'United States Minor Outlying Islands', // safe
    US: 'United States of America', // safe
    UY: 'Uruguay', // safe
    UZ: 'Uzbekistan', // safe
    VU: 'Vanuatu', // safe
    VE: 'Venezuela (Bolivarian Republic of)', // safe
    VN: 'Viet Nam', // safe
    VG: 'Virgin Islands (British)', // safe
    VI: 'Virgin Islands (U.S.)', // safe
    WF: 'Wallis and Futuna', // safe
    EH: 'Western Sahara*', // safe
    YE: 'Yemen', // safe
    ZM: 'Zambia', // safe
    ZW: 'Zimbabwe', // safe
    AX: 'Åland Islands' // safe
  },
  honorifics: {
    mr: '',
    ms: '',
    miss: '',
    mrs: '',
    mx: '',
    master: '',
    dir: '',
    madam: '',
    dame: '',
    lord: '',
    lady: '',
    viscount: '',
    dr: '',
    prof: '',
    br: '',
    sr: '',
    fr: '',
    rev: '',
    pr: '',
    elder: '',
  }
};
